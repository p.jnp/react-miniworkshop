import React, { useState, useEffect } from "react";
import HomeTable from "../../components/HomeTable";
import HomeTitle from "../../components/HomeTitle";
import HomeCreateButton from "../../components/HomeCreateButton";
import { getAllUser, deleteUser } from "../../api/api";

export default function Home(props) {
  const [user, setUser] = useState({});
  // const user = getAllUser()
  const fetchUser = async () => {
    let result = await getAllUser()
    setUser(result)
  };

  useEffect(() => {
    fetchUser()
  }, [])
  const nextCreate = () => {
    props.history.push("/create")
  }
  const removeUser = async (id) => {
    let check = window.confirm("คุณต้องการลบหรือไม่ ?")
    if(check === true) {
    let result = await deleteUser(id)
    if (result.status === "success") {
    fetchUser()
    }
    }
    }
  return (
    <div>
      <HomeCreateButton nextCreate={ nextCreate }/>
      <HomeTitle />
      <HomeTable user={user} delete={removeUser}/>
    </div>
  );
}
