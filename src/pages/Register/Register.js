import React from "react";
import { register } from "../../api/api";
import LoginForm from '../../components/LoginForm'
export default function Register(props) {
  const save = async (user) => {
    let result = await register(user)
    props.history.push('/login')
    // console.log(save);
    
  }
  return (
    <div>
      <h1 style={{ textAlign: "center" }}>REGISTER</h1>
      <div style={{ textAlign: "center" }}>
        <img
          src={process.env.PUBLIC_URL + "assets/images/2.jpg"}
          style={{ width: "250px", height: "300px", borderRadius: "50%" }}
        ></img>
      </div>
    <LoginForm save={save} />
    </div>
  );
}
