import React, { useState } from "react";
// import { login } from "../../api/api";
import LoginForm from '../../components/LoginForm'
import { loginTest } from "../../api/api";
// import "./Login.css";
export default function Login(props) {
  // const [user, setUser] = useState();
  const login = async (user) => {
    let result = await loginTest(user)
    // props.history.push('/login')
    console.log(loginTest(user));
    
  }

  return (
    <div>
      <h1 style={{ textAlign: "center", color: "#F1948A" }}>LOGIN</h1>
      <div style={{ textAlign: "center" }}>
        <img
          src={process.env.PUBLIC_URL + "assets/images/1.jpg"}
          style={{ width: "250px", height: "300px", borderRadius: "50%" }}
        ></img>
      </div>

        <LoginForm check="Login" login={login} />

    </div>
  );
}
