import React from "react";
import { Link } from "react-router-dom";
export default function HomeTable(props) {
  // console.log(props)
  const tableList = () => {
    if (props.user.data !== undefined) {
      var data = [];
      for (let i = 0; i < props.user.data.length; i++) {
        let item = props.user.data[i];
        data.push(
          <tr>
            <th scope="row">{i + 1}</th>
            <td>{item.name}</td>
            <td>{item.age}</td>
            <td>{item.salary}</td>
            <td>
            <Link to={`/edit/${item._id}`}>
                <span style={{ color: '#F1948A',backgroundColor: 'black',padding:'8px',marginRight:'2px' }}>Edit</span>
              </Link>
              <Link>
                <span onClick={() => props.delete(item._id)} style={{ color: 'black',backgroundColor: '#F1948A',padding:'8px' }}>Delete</span>
              </Link>
            </td>
          </tr>
        );
      }
      return data;
    }
  };
  return (
    <div>
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col" style={{ color: "#F1948A" }}>
              No.
            </th>
            <th scope="col" style={{ color: "#F1948A" }}>
              Name
            </th>
            <th scope="col" style={{ color: "#F1948A" }}>
              Age
            </th>
            <th scope="col" style={{ color: "#F1948A" }}>
              Salary
            </th>
            <th scope="col" style={{ color: "#F1948A" }}>
              Actions
            </th>
          </tr>
        </thead>
        <tbody>{tableList()}</tbody>
      </table>
    </div>
  );
}
