import React from "react";

export default function HomeCreateButton(props) {
  return (
    <div>
      {/* <button className="btn  btn-outline-dark float-right" style={{ marginTop: '10px' }} onClick={() => { props.nextCreate() }}>Creat User</button> */}
      <button
        type="submit"
        class="btn float-right"
        style={{ backgroundColor: 'black', color: '#F1948A' , marginTop: '10px'}}
        onClick={() => {
          props.nextCreate();
        }}
      >
        Creat User
      </button>
    </div>
  );
}
