import React from 'react'

export default function HomeTitle() {
    return (
        <div>
            <h1 style={{ color:'#FA8072'}}>All Blinks</h1>
            <hr />
        </div>
    )
}
