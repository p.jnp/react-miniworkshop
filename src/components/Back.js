import React from "react";

export default function Back(props) {
  return (
    <div>
      {/* <button className="btn  btn-outline-danger" onClick={() => { props.history.push(props.url) }}>&#706;</button> */}
      {/* <button className="btn  btn-outline-danger float-right"  style={{ marginTop: '10px' }} onClick={() => { props.history.push(props.url) }}>BACK</button> */}
      <button
        type="submit"
        class="btn  float-right"
        style={{
          backgroundColor: "#F1948A",
          color: "black",
          marginTop: "10px",
        }}
        onClick={() => {
          props.history.push(props.url);
        }}
      >
        BACK
      </button>
    </div>
  );
}
